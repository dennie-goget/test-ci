FROM alpine:3.7

COPY artifacts/app /usr/local/bin/app

ENTRYPOINT ["/usr/local/bin/app"]
